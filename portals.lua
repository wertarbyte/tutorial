local RpgPlus = require 'rpgplus'
local PortalsModule = {}
local portals = {}

function PortalsModule.once(location, cb)
  local fn
  fn = function (e)
    local loc = e:getPlayer():getLocation()
    if loc:getBlockX() == location.x and
       loc:getBlockY() == location.y and
       loc:getBlockZ() == location.z and
       loc:getWorld():getName() == location.world then
      RpgPlus.off('player.move', fn)
      cb(e)
    end
  end
  RpgPlus.on('player.move', fn)
end

return PortalsModule
