local RpgPlus = require 'rpgplus'
local Scheduler = require 'rpgplus.scheduler'
local Entities = require 'rpgplus.entities'
local Portals = require './portals'

local demoVillager = Entities.spawn('villager', {
  world = 'world',
  x = -214.5,
  y = 70,
  z = 467,
  yaw = -90,
  pitch = 0,
  invulnerable = true,
  name = 'Heinrich'
})

local christian = Entities.spawn('villager', {
  world = 'world',
  x = -214.5,
  y = 74,
  z = 466,
  yaw = -90,
  pitch = 0,
  invulnerable = true,
  name = 'Christian'
})

demoVillager:on('player.interact.entity', function (e)
  local respond = function (str) demoVillager:tell(e:getPlayer(), str) end
  e:setCancelled(true)
  demoVillager:startDialog(e:getPlayer(), {
    {0, 'Hallo ' .. e:getPlayer():getName() .. '! Worüber möchtest du mehr erfahren?', { 2 }, { delay=60 }},
    {2, 'Survival-Welt', '2a'},
    {'2a', 'Eine gute Wahl!', '2a1', { delay=20 }},
    {'2a1', function ()
      respond('CHRISTIAAAN! Hier will jemand was über die Survival-Welt wissen.')
      Scheduler.delay(40, function ()
        christian:tell(e:getPlayer(), 'Was? Ist gerade schlecht...')
      end)
      Scheduler.delay(60, function ()
        respond('Wie, du kannst jetzt nicht? BEWEG DEINEN *RSCH HIER HER!')
      end)
      Scheduler.delay(80, function ()
        christian:navigateTo({ x = -212, y = 70, z = 464, pitch = -62, world = 'world', openDoors = true })

        Scheduler.delay(60, function ()
          christian:tell(e:getPlayer(), 'Junge, junge! Ich frage mich, warum Heinrich noch nicht gebannt wurde.')
        end)

        Scheduler.delay(120, function ()
          christian:tell(e:getPlayer(), 'Folge mir, ' .. e:getPlayer():getName() .. '!')
          christian:navigateTo({ x = -213, y = 70, z = 459, world = 'world', openDoors = true })
          Portals.once({ x = -214, y = 70, z = 459, world = 'world' }, function (e)
            RpgPlus.sendMessage(e:getPlayer(), 'Fühle dich teleportiert.')
          end)
        end)
      end)
    end}
  })
end)

--[[
RpgPlus.on('player.chat', function (e)
  if e:getMessage():find('Heinrich') or e:getMessage():find('heinrich') then
    e:setCancelled(true)
    Scheduler.delay(1, function ()
      demoVillager:tell(e:getPlayer(), {'Ja, sofort!', 'Bin unterwegs!', 'Ich komme!'})
      demoVillager:navigateTo(e:getPlayer():getLocation())
    end)
  end
end)
]]--